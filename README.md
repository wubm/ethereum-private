# Ethereum

# Pre-install
+ Docker version 17.05.0-ce
+ Docker-compose version 1.13.0
+ Git

# Dr.Chain
### prepare environment
1. Execute ```bash install.sh``` to prepare environment.

### Execute
1. ```docker-compose up -d``` to start container(mres, ethereum, explorer, ipfs)

### Port mapping
| container_name     | service          | port        |
| :----------------- |:---------------- | :---------- |
| ethereum           | p2p              | 30303       |
| ethereum           | rpc              | 8545        |
| explorer           | explorer         | 3000        |
| browser-solidity   | browser-solidity | 8080        |

### Key, generated from https://iancoleman.github.io/bip39/#english
|1           | init balance : 100000                                                                                           |
|:-----------|:--------------------------------------------------------------------------------------------------------|
|Mnemonic    | intact patch melody crater exhaust fabric stool dragon fire potato sting fix online rubber exile        |
|Path        | m/44'/60'/0'/0/0                                                                                        |
|Address     | 0xdE3f5C9E3eA411DA3E344C04461D14219da2BF75                                                              |
|Public Key  | 0x0215ea1a68aa5b25c4e986d2b0a8b2797d0912c826bcaa0c9e6afb154978e3cb09                                    |
|Private Key | 0xeb1725ffad1c889077cf6d71bf817553a94318164b1775a09e75b7d86740058a                                      |

|2           | init balance : 200000                                                                                  |
|:-----------|:--------------------------------------------------------------------------------------------------------|
|Mnemonic    | settle yard cinnamon kind fortune palace animal ribbon diet govern chicken raise wisdom broom tonight   |
|Path        | m/44'/60'/0'/0/0                                                                                        |
|Address     | 0xe6dfCb24557911135fF4B97C4C7B882274f7753C                                                              |
|Public Key  | 0x0395d79b864324f4f60b9b9a5bc3d091273f186b7aa8dcfd3b56025f95b0ebee91                                    |
|Private Key | 0xced62ee96650913ce0c9efd65c5cff7d950d7c5dfaf01e76af191d6d0eb50082                                      |

|3           | init balance : 300000                                                                                  |
|:-----------|:--------------------------------------------------------------------------------------------------------|
|Mnemonic    | panda elegant multiply few team ring rebuild firm trust future public dinosaur voyage mystery adjust    |
|Path        | m/44'/60'/0'/0/0                                                                                        |
|Address     | 0x5bDC959dc1A27CebF2CA2912413B387cCe73f667                                                              |
|Public Key  | 0x02831ada515f45bd2194aee8fc0db3a0591f548a2aaa521d4f12939258fee295ec                                    |
|Private Key | 0x56e1c0c6882387d98477baccaa0415472334c16a10a284f1dcb63ea2a3fdc23f                                      |

|4           | init balance : 400000                                                                                  |
|:-----------|:--------------------------------------------------------------------------------------------------------|
|Mnemonic    | address speed resource below field earth naive claw rescue spoon eager danger million shoot cry         |
|Path        | m/44'/60'/0'/0/0                                                                                        |
|Address     | 0x63aAcD55E78E7C86B91333c72D4887e4732ce75a                                                              |
|Public Key  | 0x02f92aae69cbf32801283a33786b6ef074dbfff534024fcb5e9d57bb413680612e                                    |
|Private Key | 0x725ec77442d0fcf85258b4d67cc7571340c064e0c3f94ad50cd953f75817c424                                      |

|5           | init balance : 500000                                                                                  |
|:-----------|:--------------------------------------------------------------------------------------------------------|
|Mnemonic    | giraffe tooth rubber over visit enact subject common transfer color dash mail coyote diagram plunge     |
|Path        | m/44'/60'/0'/0/0                                                                                        |
|Address     | 0x8aEb36598FBcD2873AB31dc8971773227792ef71                                                              |
|Public Key  | 0x0364846e0cdfce73b6cb64b3d0363672d55e7b9ffc21193dab98ed1f82a99ce5d7                                    |
|Private Key | 0x282976a4da8a3b5e1e8bce6ec6b3c4cefc79b4eac213458d86e977e4152da5e9                                      |
