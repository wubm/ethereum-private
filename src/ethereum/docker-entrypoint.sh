#!/bin/bash

CONF_FILE="/etc/ethereum/configure.conf"
source $CONF_FILE
RULE="$1"

change_ethereum_setting() {
    sed -i "s#\[DATA_DIR\]#$ETHEREUM_DATA_DIR#g" $ETHEREUM_SUPERVISORD_CONF
    sed -i "s#\[NETWORK_ID\]#$ETHEREUM_NETWORK_ID#g" $ETHEREUM_SUPERVISORD_CONF
    sed -i "s#\[P2P_PORT\]#$ETHEREUM_P2P_PORT#g" $ETHEREUM_SUPERVISORD_CONF
    sed -i "s#\[PRC_LISTEM_ADD\]#$ETHEREUM_PRC_LISTEM_ADD#g" $ETHEREUM_SUPERVISORD_CONF
    sed -i "s#\[RPC_PORT\]#$ETHEREUM_RPC_PORT#g" $ETHEREUM_SUPERVISORD_CONF
    sed -i "s#\[RPC_API\]#$ETHEREUM_RPC_API#g" $ETHEREUM_SUPERVISORD_CONF
    sed -i "s#\[LOG_DIR\]#$LOG_DIR#g" $ETHEREUM_SUPERVISORD_CONF
}

if [ -z "$ETHEREUM_DATA_DIR" ] ; then
    ETHEREUM_DATA_DIR="/root/ethereum/node"
fi
if [ -z "$ETHEREUM_SUPERVISORD_CONF" ] ; then
    ETHEREUM_SUPERVISORD_CONF="/etc/supervisor/conf.d/geth.conf"
fi
if [ -z "$ETHEREUM_NETWORK_ID" ] ; then
    ETHEREUM_NETWORK_ID="9487"
fi
if [ -z "$ETHEREUM_P2P_PORT" ] ; then
    ETHEREUM_P2P_PORT="30303"
fi
if [ -z "$ETHEREUM_PRC_LISTEM_ADD" ] ; then
    ETHEREUM_PRC_LISTEM_ADD="0.0.0.0"
fi
if [ -z "$ETHEREUM_RPC_PORT" ] ; then
    ETHEREUM_RPC_PORT="8545"
fi
if [ -z "$ETHEREUM_RPC_API" ] ; then
    ETHEREUM_RPC_API="web3,eth"
fi

if [ -f /initialization ] && [ `cat /initialization` == "1" ]; then
    echo "$(date +'[%d/%b/%Y %T]') Start Ethereum" >> $LOG_DIR/ethereum.log
    geth --datadir $ETHEREUM_DATA_DIR --networkid $ETHEREUM_NETWORK_ID \
         --port $ETHEREUM_P2P_PORT --rpc --rpcaddr $ETHEREUM_PRC_LISTEM_ADD \
         --rpcport $ETHEREUM_RPC_PORT --rpcapi $ETHEREUM_RPC_API \
         1>/$LOG_DIR/ethereum.log 2>$LOG_DIR/ethereum.err.log &
    sleep 3
    pkill -f geth
    sleep 2
    #echo "$(date +'[%d/%b/%Y %T]') Set miner" >> $LOG_DIR/ethereum.log
    #if [ "$RULE" == "--firstnode" -o "$RULE" == "-f" ] ; then
    #    address=$(cat /root/address.txt)
    #    bash /usr/local/bin/set_miner.sh $address
    #elif [ "$RULE" != "--firstnode" -o "$RULE" != "-f" ] ; then
    #    echo "$(date +'[%d/%b/%Y %T]') Join to pool" >> $LOG_DIR/ethereum.log
    #    bash /usr/local/bin/join_pool.sh
    #fi
    exec /usr/bin/supervisord

else
    echo "$(date +'[%d/%b/%Y %T]') Start Init Ethereum" >> $LOG_DIR/ethereum.log
    geth --datadir $ETHEREUM_DATA_DIR init /root/ethereum/genesis.json
    address=$(bash /usr/local/bin/create_account.sh \
              | awk -F"{" '{ print $2 }' | sed "s/}//g" )
    echo $address >> /root/address.txt
    echo "$(date +'[%d/%b/%Y %T]') Finished Init Ethereum" >> $LOG_DIR/ethereum.log
    echo "$(date +'[%d/%b/%Y %T]') Change supervisord config" >> $LOG_DIR/ethereum.log
    change_ethereum_setting

    if [ ! -f /initialization ]; then
        touch /initialization
    else
        sed -i '1,$d' /initialization
    fi
    echo "1" > /initialization
fi
