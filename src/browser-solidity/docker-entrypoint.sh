#!/bin/bash
CONF_FILE="/etc/browser-solidity/configure.conf"
source $CONF_FILE
BROWER_SOLIDITY_DIR="/root/browser-solidity"
LOG_DIR="/var/log/browser-solidity"

change_config() {
    cd $BROWER_SOLIDITY_DIR
    sed -i "s/localhost/$ETHEREUM_HOST/g" $BROWER_SOLIDITY_DIR/tools/grabber.js
    sed -i "s/8545/$ETHEREUM_RPC_PORT/g" $BROWER_SOLIDITY_DIR/tools/config.json
    sed -i "s/2000000/0/g" $BROWER_SOLIDITY_DIR/tools/config.json
    sed -i "s/localhost:8545/$ETHEREUM_HOST:$ETHEREUM_RPC_PORT/g" $BROWER_SOLIDITY_DIR/routes/web3relay.js
}

main () {
    cd $BROWER_SOLIDITY_DIR
    if [ -f /initialization ] && [ `cat /initialization` == "1" ] ; then
        echo "Waitting ethereun ready." >> $LOG_DIR/explorer.log
        until curl $ETHEREUM_HOST:$ETHEREUM_RPC_PORT 1>>/dev/null 2>>/dev/null ; do
            sleep 1
            printf "." >> $LOG_DIR/browser-solidity.log
        done
        sleep 10
        echo "Start browser-solidity" $LOG_DIR/browser-solidity.log
        cd $BROWER_SOLIDITY_DIR
        npm start 1>>$LOG_DIR/browser-solidity.log 2>>$LOG_DIR/browser-solidity.err.log &
        exec /sbin/init
    else
        change_config
        echo "1" > /initialization
    fi
}

main "$@"
